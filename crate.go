package crate

import (
	"encoding/json"

	"gitlab.com/ashinnv/oddstring"
)

// Not exported. Getters and setters. Carries whole submission from one upload.
type crate struct {
	Id        string
	Timestamp uint64
	Values    []box
}

// Carries each related individual value set
type box struct {
	Id    string      `json:"Id"`
	Key   string      `json:"Key"`
	Value interface{} `json:"Value"`
}

// Return a json object represending the crate. More to come later
func (crt *crate) GetBoxes() (string, error) {
	if ret, err := json.Marshal(crt); err != nil {
		return "", err
	} else {
		return string(ret), nil
	}
}

func BuildBox(jsonData string) (box, error) {

	var retBox box
	if err := json.Unmarshal([]byte(jsonData), &retBox); err != nil {
		return box{}, err
	} else {
		return retBox, nil
	}

}

func BuildCrate(timestamp uint64, input []box) (crate, error) {
	retCrate := crate{
		genId(),
		timestamp,
		input,
	}

	return retCrate, nil //We need to have error checking
}

func genId() string {
	//osm := oddstring.OddstringMode{true, false, false, false, false}
	return string(oddstring.RandStringSingle(25))
}
