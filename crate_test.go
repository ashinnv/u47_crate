package crate

import (
	"encoding/json"
	"fmt"
	"os"
	"testing"
	"time"
)

func TestShortHand(t *testing.T) {
	fmt.Println("\n\nTEST SHORTHAND")
	dat, err := os.ReadFile("test.json")
	if err != nil {
		fmt.Println("nil: ", err)
	}

	bx, err2 := BuildBox(string(dat))
	if err2 != nil {
		fmt.Println("ERR2:", err2)
	}
	fmt.Println("BX__1:", bx)

}

func TestFullCrate(t *testing.T) {
	fmt.Println("\n\nTESTFULLCRAATE")
	var tmp crate
	dat, _ := os.ReadFile("test.json")
	json.Unmarshal(dat, &tmp)
	fmt.Println("TMP", tmp)
}

func TestCrateCreation(t *testing.T) {
	fmt.Println("\n\n CRATE CREATION")

	//rstr := oddstring.RandStringSingle(14)
	data, _ := os.ReadFile("test.json")
	bx, _ := BuildBox(string(data))
	fmt.Println("BX:", bx)

	data, _ = os.ReadFile("test2.json")
	bx2, _ := BuildBox(string(data))
	fmt.Println("BX2:", bx2)

	data, _ = os.ReadFile("test3.json")
	bx3, _ := BuildBox(string(data))
	fmt.Println("BX3:", bx3)
	fmt.Println()

	crt, _ := BuildCrate(uint64(time.Now().Unix()), []box{bx, bx2, bx3})
	fmt.Println("CRATE:", crt)
}
